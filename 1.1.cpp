#include "geometry_msgs/Twist.h" 
#include "tf/transform_listener.h" // tf Tree

#include <iostream>
#include <cmath>
#include <std_msgs/Float64.h>
#include <algorithm>
#include <stack>

ros::Subscriber der1, der2, der3, der4, der5, der6;

ros::Publisher array_publisher; 

boolean kontrol [6] = {0, 0, 0, 0, 0, 0};

void gonderi_kontrol()
{
    if (kontrol[0] && 
        kontrol[1] &&
        kontrol[2] &&
        kontrol[3] &&
        kontrol[4] &&
        kontrol[5])
    {
        //array_publisher.publish(gonderi);

        kontrol[0] = 0; 
        kontrol[1] = 0; 
        kontrol[2] = 0; 
        kontrol[3] = 0; 
        kontrol[4] = 0; 
        kontrol[5] = 0; 
    }
}

radyana_cevir(const int derece)
{
   pi = 3.14158;
   radian;

    radian = pi * derece /180 ;
    return (radian_var);
}

void callback1(const int angle)
{
    kontrol[0] = 1;
    gonderi[0] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}

void callback2(const int angle)
{
    kontrol[1] = 1;
    gonderi[1] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}

void callback3(const int angle)
{
    kontrol[2] = 1;
    gonderi[2] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}
	
void callback4(const int angle)
{
    kontrol[3] = 1;
    gonderi[3] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}
	
void callback5(const int angle)
{
    kontrol[4] = 1;
    gonderi[4] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}
	
void callback6(const int angle)
{
    kontrol[5] = 1;
    gonderi[5] = radyana_cevir(angle);
    gonderi_kontrol();
    return;
}
	

int main(int argc, char** argv)
{

	ros::init(argc, argv, "back");
	
	ros::NodeHandle n;
	

    der1 = n.subscribe("/standing2/JointState1", 1000, callback1);
    der2 = n.subscribe("/standing2/JointState2", 1000, callback2);
    der3 = n.subscribe("/standing2/JointState3", 1000, callback3);
    der4 = n.subscribe("/standing2/JointState4", 1000, callback4);
    der5 = n.subscribe("/standing2/JointState5", 1000, callback5);
    der6 = n.subscribe("/standing2/JointState6", 1000, callback6);

    array_publisher = n.advertise<geometry_msgs::Twist>("/cmd_vel", 100);

    ros::Duration time_between_ros_wakeups(0.001);
    return 0;
}